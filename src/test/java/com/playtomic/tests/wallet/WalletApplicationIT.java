package com.playtomic.tests.wallet;

import com.playtomic.tests.wallet.api.WalletController;
import com.playtomic.tests.wallet.service.WalletService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class WalletApplicationIT {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Mock
	protected WalletService walletService;

	@InjectMocks
	private WalletController walletController;

	@Before
	public void setup() throws Exception {
		if (this.mockMvc == null) {
			this.mockMvc = MockMvcBuilders
					.standaloneSetup(walletController)
					.build();
		}
	}

	protected MockMvc getMockMvc() {
		return this.mockMvc;
	}

	@Test
	public void emptyTest() {
	}
}
