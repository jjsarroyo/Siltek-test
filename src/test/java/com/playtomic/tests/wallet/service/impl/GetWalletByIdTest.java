package com.playtomic.tests.wallet.service.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.playtomic.tests.wallet.WalletApplicationIT;
import com.playtomic.tests.wallet.dto.WalletDto;
import java.util.Optional;
import org.junit.Test;
import org.springframework.http.MediaType;

public class GetWalletByIdTest extends WalletApplicationIT {

    @Test
    public void testOk() throws Exception{

        WalletDto wallet = new WalletDto(1L, 35.0f);
        when(walletService.getWallet(1L)).thenReturn(Optional.of(wallet));
        getMockMvc().perform(get("/wallet/{id}", 1L))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$.id").value(1l))
            .andExpect(jsonPath("$.amount").value(35.0f));
        verify(walletService, times(1)).getWallet(1L);
        verifyNoMoreInteractions(walletService);
    }
}
