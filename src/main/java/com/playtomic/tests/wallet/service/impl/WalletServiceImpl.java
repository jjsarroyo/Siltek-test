package com.playtomic.tests.wallet.service.impl;

import com.playtomic.tests.wallet.domain.Wallet;
import com.playtomic.tests.wallet.dto.WalletDto;
import com.playtomic.tests.wallet.repository.WalletRepository;
import com.playtomic.tests.wallet.service.WalletService;
import java.util.Optional;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WalletServiceImpl implements WalletService {

  // constructor injection allows the repository field to be marked as final, indicating that it cannot be subsequently changed.
  private final WalletRepository repository;

  private Logger log = LoggerFactory.getLogger(WalletServiceImpl.class);

  @Autowired
  public WalletServiceImpl(WalletRepository repository) {
    this.repository = repository;
  }

  @Autowired
  DozerBeanMapper dozerBeanMapper;

  @Override
  public Optional<WalletDto> getWallet(Long id) {

    Optional<Wallet> wallet = repository.findOne(id);
    if (wallet.isPresent())
      return Optional.of(dozerBeanMapper.map(repository.findOne(id).get(), WalletDto.class));
    else
      return Optional.empty();
  }

  @Override
  public Optional<WalletDto> chargeAmount(Long id, Float amount) {

    Optional<Wallet> oldWallet = repository.findOne(id);

    if (oldWallet.isPresent()) {
      return Optional.of(dozerBeanMapper.map(
          repository.save(
              new Wallet(oldWallet.get().getId(), oldWallet.get().getAmount() - amount)), WalletDto.class));
    } else {
      return Optional.empty();
    }

  }

  @Override
  public Optional<WalletDto> refundAmount(Long id, Float amount) {

    Optional<Wallet> oldWallet = repository.findOne(id);

    if (oldWallet.isPresent()) {
      return Optional.of(dozerBeanMapper.map(
          repository.save(
              new Wallet(oldWallet.get().getId(), oldWallet.get().getAmount() + amount)), WalletDto.class));
    } else {
      // Business rule -> refund creates wallet if it does not exists
      return Optional.of(dozerBeanMapper.map(
          repository.save(new Wallet(id,amount)),WalletDto.class));

    }
  }

}
