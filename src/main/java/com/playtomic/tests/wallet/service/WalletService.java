package com.playtomic.tests.wallet.service;

import com.playtomic.tests.wallet.dto.WalletDto;
import java.util.Optional;

public interface WalletService {

  Optional<WalletDto> getWallet(Long id);

  Optional<WalletDto> chargeAmount(Long id, Float amount);

  Optional<WalletDto> refundAmount(Long id, Float amount);
}
