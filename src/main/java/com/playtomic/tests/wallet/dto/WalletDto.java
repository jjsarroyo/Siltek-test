package com.playtomic.tests.wallet.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class WalletDto {

  private Long id;

  private Float amount;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Float getAmount() {
    return amount;
  }

  public void setAmount(Float amount) {
    this.amount = amount;
  }

  public WalletDto(Long id, Float amount) {
    this.id = id;
    this.amount = amount;
  }

  public WalletDto() {
  }
}
