package com.playtomic.tests.wallet.api;

import com.playtomic.tests.wallet.dto.WalletDto;
import com.playtomic.tests.wallet.exception.AccountNotFoundException;
import com.playtomic.tests.wallet.exception.PaymentServiceException;
import com.playtomic.tests.wallet.service.WalletService;
import com.playtomic.tests.wallet.service.impl.ThirdPartyPaymentService;
import java.math.BigDecimal;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WalletController {
    private Logger log = LoggerFactory.getLogger(WalletController.class);

    private final WalletService walletService;

    private final ThirdPartyPaymentService thirdPartyPaymentService;

    @Autowired
    public WalletController(WalletService walletService, ThirdPartyPaymentService thirdPartyPaymentService) {
        this.walletService = walletService;
        this.thirdPartyPaymentService = thirdPartyPaymentService;
    }

    @RequestMapping("/")
    void log() {
        log.info("Logging from /");
    }

    @GetMapping("/wallet/{id}")
    public WalletDto getWallet(@PathVariable("id") Long id) throws AccountNotFoundException {

      Optional<WalletDto> responseDto = walletService.getWallet(id);
      if (responseDto.isPresent())
          return responseDto.get();
      else {
        log.error("account with id {} not found", id);
        throw new AccountNotFoundException();
      }
    }

    @GetMapping("wallet/{id}/charge")
    public WalletDto chargeAmount(@PathVariable("id") Long id, @RequestParam Float amount)
        throws AccountNotFoundException{

      Optional<WalletDto> responseDto =  walletService.chargeAmount(id,amount);
      if (responseDto.isPresent())
        return responseDto.get();
      else {
        log.error("account with id {} not found", id);
        throw new AccountNotFoundException();
      }
    }

    @GetMapping("wallet/{id}/refund")
    public WalletDto refundAmount(@PathVariable("id") Long id, @RequestParam Float amount) {

        return walletService.refundAmount(id,amount).get();
    }

    @GetMapping("wallet/{id}/externalCharge")
    public WalletDto chargeAmountThirdParties(@PathVariable("id") Long id, @RequestParam BigDecimal amount)
        throws AccountNotFoundException,PaymentServiceException {

        thirdPartyPaymentService.charge(amount);
        // Since third parties services is a dummy , wallet amount won't change. I return the wallet
        // like in the rest of the endpoints, when dummy is implemented this should work well, right now
        // it returns the wallet with the same amount
        Optional<WalletDto> responseDto = walletService.getWallet(id);
        if (responseDto.isPresent())
          return responseDto.get();
        else {
          log.error("account with id {} not found", id);
          throw new AccountNotFoundException();
        }

    }
}
