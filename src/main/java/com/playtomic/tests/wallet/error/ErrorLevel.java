package com.playtomic.tests.wallet.error;

public enum ErrorLevel {
    INFO,
    WARNING,
    ERROR
}
