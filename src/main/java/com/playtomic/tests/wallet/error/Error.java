package com.playtomic.tests.wallet.error;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.http.HttpStatus;

public enum Error {
    REQUEST_METHOD_NOT_SUPPORTED(HttpStatus.METHOD_NOT_ALLOWED.value(), "request_method_not_supported", ErrorLevel.INFO, "Request method not supported.", ""),
    ACCOUNT_NOT_FOUND(HttpStatus.NOT_FOUND.value(), "Account not found", ErrorLevel.INFO, "Account not found.", "Server logs may have additional information."),
    EXTERNAL_PAYMENT_ERROR(HttpStatus.BAD_REQUEST.value(), "minimum amount is 10", ErrorLevel.INFO, "minimum amount is 10", "Server logs may have additional information."),

    BAD_REQUEST(HttpStatus.BAD_REQUEST.value(), "bad_request", ErrorLevel.INFO, "Request is malformed or there are missing mandatory parameters.", ""),
    UNEXPECTED_ERROR(HttpStatus.INTERNAL_SERVER_ERROR.value(), "internal_server_error", ErrorLevel.ERROR, "Internal server error. Server logs may have additional information.", "");

    private final int code;
    private final String message;
    private final ErrorLevel level;
    private final String description;
    private final String moreInfo;

    Error(int code, String message, ErrorLevel level, String description, String moreInfo) {
        this.code = code;
        this.message = message;
        this.level = level;
        this.description = description;
        this.moreInfo = moreInfo;
    }

    /**
     * Returns the integer code of the error.
     *
     * @return code
     */
    public int getCode() {
        return code;
    }

    /**
     * Returns the message of the error.
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Returns the level of the error.
     *
     * @return level
     */
    public ErrorLevel getLevel() {
        return level;
    }

    /**
     * Returns the error description
     *
     * @return description
     */
    public String getDescription() {return description;}

    /**
     * Returns more info
     *
     * @return moreInfo
     */ 
    public String getMoreInfo() {return moreInfo;}

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("code", code)
                .append("message", message)
                .append("level", level)
                .append("description", description)
                .append("moreInfo", moreInfo)
                .toString();
    }
}
