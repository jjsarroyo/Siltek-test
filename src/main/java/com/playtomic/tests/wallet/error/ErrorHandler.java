package com.playtomic.tests.wallet.error;

import com.playtomic.tests.wallet.exception.AccountNotFoundException;
import com.playtomic.tests.wallet.exception.PaymentServiceException;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class ErrorHandler {

    private Logger log = LoggerFactory.getLogger(ErrorHandler.class);

    @ExceptionHandler({AccountNotFoundException.class})
    public ErrorResponse exceptionHandler(HttpServletResponse response) {
        ErrorResponse errorResponse = new ErrorResponse(Error.ACCOUNT_NOT_FOUND);
        response.setStatus(Error.ACCOUNT_NOT_FOUND.getCode());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        return errorResponse;
    }

  @ExceptionHandler({PaymentServiceException.class})
  public ErrorResponse PaymentExceptionHandler(HttpServletResponse response) {
    ErrorResponse errorResponse = new ErrorResponse(Error.EXTERNAL_PAYMENT_ERROR);
    response.setStatus(Error.EXTERNAL_PAYMENT_ERROR.getCode());
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    return errorResponse;
  }
}
