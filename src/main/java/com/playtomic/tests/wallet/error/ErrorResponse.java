package com.playtomic.tests.wallet.error;

public class ErrorResponse {
    private int code;
    private String message;
    private ErrorLevel level;
    private String description;
    private String moreInfo;

    ErrorResponse(Error error) {
        this.code = error.getCode();
        this.message = error.getMessage();
        this.level = error.getLevel();
        this.description = error.getDescription();
        this.moreInfo = error.getMoreInfo();
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public ErrorLevel getLevel() {
        return level;
    }

    public String getDescription() {
        return description;
    }

    public String getMoreInfo() {
        return moreInfo;
    }
}
