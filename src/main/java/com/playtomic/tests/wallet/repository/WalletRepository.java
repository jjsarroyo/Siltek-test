package com.playtomic.tests.wallet.repository;

import com.playtomic.tests.wallet.domain.Wallet;
import java.util.Optional;
import org.springframework.data.repository.Repository;

public interface WalletRepository extends Repository<Wallet,Long> {

  // I extend repository instead of CRUD repository for 2 reasons
  // 1.- When we create an interface, we should not add unnecessary methods to it. We should keep the interface as small as possible
  // 2.- Optional helps us to create better APIs because it reveals that there might not be a return value. CRUD repository does not offer Optional
  Optional<Wallet> findOne(Long id);

  Wallet save(Wallet p0);
}

