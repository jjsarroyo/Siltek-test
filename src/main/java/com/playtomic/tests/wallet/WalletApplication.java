package com.playtomic.tests.wallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WalletApplication {

	public static void main(String[] args) {
		SpringApplication.run(WalletApplication.class, args);
	}

	// example entrypoints
	// Create wallet
	// http://localhost:8090/wallet/1/refund?amount=20
	// Charge wallet
	// http://localhost:8090/wallet/1/charge?amount=20
	// refund wallet again
	// http://localhost:8090/wallet/1/refund?amount=30
	// third party payment
	// http://localhost:8090/wallet/1/externalCharge?amount=10
}
